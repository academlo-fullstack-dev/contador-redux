import { createStore, combineReducers, applyMiddleware } from "redux";
import logger from "redux-logger";
import contadorReducer from "./reducers/contador-reducer";

//Combinar todos los reducers -> es como si estuvieramos combinando todos los estados de nuestra aplicacion en uno solo

/*
    const [pokemones, setPokemones] = useState([]);
    const [user, setUser] = useState({});

    al combinar estos estados -> 

    store: {
        pokemones: [],
        user: {}
    }
*/

const rootReducer = combineReducers({ contadorReducer });

//Construimos nuestro store con la combinacion de todos los reducers y le pasamos un estado inicial
const store = createStore(rootReducer, applyMiddleware(logger));

export default store;