export const aumentar = () => {
    return {
        type: "AUMENTAR"
    }
}

export const disminuir = () => {
    return {
        type: "DISMINUIR"
    }
}

export const multiplicar = () => {
    return {
        type: "MULTIPLICAR"
    }
}

export const elevar = () => {
    return {
        type: "ELEVAR"
    }
}

export const reset = () => {
    return {
        type: "RESET"
    }
}