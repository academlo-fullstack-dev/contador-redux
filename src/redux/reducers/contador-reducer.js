const INITIAL_STATE = 0;

const ContadorReducer = (state = INITIAL_STATE, action) => {
    //AUMENTAR, DISMINUIR, MULTIPLICAR, ELEVAR Y RESET
    switch(action.type){
        case "AUMENTAR":
            return state + 1;
        case "DISMINUIR":
            return state - 1;
        case "MULTIPLICAR":
            return state * 3;
        default: 
            return state;
    }
}

export default ContadorReducer;