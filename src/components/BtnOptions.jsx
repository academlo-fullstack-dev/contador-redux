import React, {useEffect, memo} from "react";
import {disminuir, aumentar, multiplicar} from "../redux/actions/contador";
import {useDispatch} from "react-redux";

//Store dispatch sirve para enviar la accion al reducer
//El reducer va a decir que hacer con esa accion gracias al switch
const BtnOptions = memo(() => {
    const dispatch = useDispatch();

    useEffect(() => {
      console.log("El componente BtnOptions se ha renderizado/actualizado");
    });

    return (
      <div className="App">
        <div className="btn btn-prev" onClick={() => dispatch(disminuir())}>
          -
        </div>
        <div className="btn btn-next" onClick={() => dispatch(aumentar())}>
          +
        </div>
        <div className="btn btn-next" onClick={() => dispatch(multiplicar())}>
          Multiplicar
        </div>
        <div className="btn btn-next" onClick={() => {}}>
          ^2
        </div>
        <div className="btn btn-reset" onClick={() => {}}>
          RESET
        </div>
      </div>
    );
});

export default BtnOptions;