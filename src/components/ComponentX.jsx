import React, {useEffect} from "react";
import {connect} from "react-redux";

function ComponentX(){

    useEffect(() => {
        console.log("El componente x se ha renderizado/actualizado");
    });

    return (
        <div>
            El valor del contador es: 
        </div>
    )
}

const mapStateToProps = state => {
    return {
        contador: state.contadorReducer
    }
}


// export default connect(mapStateToProps)(ComponentX);
export default ComponentX;