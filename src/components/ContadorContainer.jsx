import React, {useEffect} from "react";
import BtnOptions from "./BtnOptions";
import { useSelector } from "react-redux"; //Higher Order Component HOC 

function ContadorContainer(props){

    const contador = useSelector((state) => state.contadorReducer);

    useEffect(() => {
        console.log("El componente ContadorContainer se ha renderizado/actualizado");
    });

    return (
        <div className="container">
            <h1>Contador</h1>
            <div className="counter-container">
                <h1 className="count">{contador}</h1>
                {/* Incluir el componente BtnOptions */}
                <BtnOptions />
            </div>
        </div>
    )
}

export default ContadorContainer;