import React from "react";
import ContadorContainer from "./components/ContadorContainer";
import ComponentX from "./components/ComponentX";
import './App.css';

function App() {
  return (
    <div className="App">
      <ContadorContainer />
      <ComponentX />
    </div>
  );
}

export default App;
